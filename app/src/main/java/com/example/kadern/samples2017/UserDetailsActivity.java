package com.example.kadern.samples2017;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.kadern.samples2017.models.User;

public class UserDetailsActivity extends AppCompatActivity {

    public static final String USER_ID_EXTRA = "userid";

    AppClass app;
    User user;
    EditText txtFirstName;
    EditText txtEmail;
    RadioGroup rgFavoriteMusic;
    CheckBox chkActive;
    Button btnSave;

    Button btnClearForm;
    Button btnInsertData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);

        app = (AppClass)getApplication();

        txtFirstName = (EditText)findViewById(R.id.txtFirstName);
        txtEmail = (EditText)findViewById(R.id.txtEmail);
        rgFavoriteMusic = (RadioGroup)findViewById(R.id.rgFavoriteMusic);
        chkActive = (CheckBox)findViewById(R.id.chkActive);
        btnSave = (Button)findViewById(R.id.btnSave);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDataFromUI();
            }
        });

        btnClearForm = (Button)findViewById(R.id.btnClearForm);
        btnClearForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetForm();
            }
        });

        btnInsertData = (Button)findViewById(R.id.btnInsertData);
        btnInsertData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                putDataInUI();
            }
        });

        Intent i = getIntent();
        long userId = i.getLongExtra(USER_ID_EXTRA,-1);
        if(userId >= 0){
            //Toast.makeText(this, "GET USER: " + userId, Toast.LENGTH_LONG).show();
            user = app.getUserById(userId);
            putDataInUI();
        }
    }

    private void getDataFromUI(){

        String firstName = txtFirstName.getText().toString();
        String email = txtEmail.getText().toString();
        boolean active = chkActive.isChecked();
        // Here's how we can set the favorite music...
        User.Music favoriteMusic = null;
        int selectedRadioButtonId = rgFavoriteMusic.getCheckedRadioButtonId();

        switch(selectedRadioButtonId){
            case R.id.rbCountry:
                favoriteMusic = User.Music.COUNTRY;
                break;
            case R.id.rbRap:
                favoriteMusic = User.Music.RAP;
                break;
            case R.id.rbJazz:
                favoriteMusic = User.Music.JAZZ;
                break;
        }

        user = new User(1, firstName, email, favoriteMusic, active);
        Toast.makeText(this, user.toString(), Toast.LENGTH_LONG).show();
    }

    private  void putDataInUI(){

        txtFirstName.setText(user.getFirstName());
        txtEmail.setText(user.getEmail());
        chkActive.setChecked(user.isActive());

        switch(user.getFavoriteMusic()){
            case COUNTRY:
                rgFavoriteMusic.check(R.id.rbCountry);
                break;
            case JAZZ:
                rgFavoriteMusic.check(R.id.rbJazz);
                break;
            case RAP:
                rgFavoriteMusic.check(R.id.rbRap);
                break;
        }
    }

    private void resetForm(){
        rgFavoriteMusic.clearCheck();
        txtFirstName.setText("");
        txtEmail.setText("");
        chkActive.setChecked(false);
    }
}

package com.example.kadern.samples2017;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kadern.samples2017.models.User;

public class UserListActivity extends AppCompatActivity {

    AppClass app;
    ListView userListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);

        app = (AppClass)getApplication();

        userListView = (ListView)findViewById(R.id.userListView);

        /*
        ArrayAdapter<User> adapter = new ArrayAdapter<User>(this, android.R.layout.simple_list_item_1, app.users);

        userListView.setAdapter(adapter);

        userListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                User selectedUser = app.users.get(position);
                //Toast.makeText(UserListActivity.this, selectedUser.getFirstName(),Toast.LENGTH_SHORT).show();
                Intent i = new Intent(UserListActivity.this, UserDetailsActivity.class);
                i.putExtra(UserDetailsActivity.USER_ID_EXTRA, position);
                startActivity(i);
            }
        });
        */

        ArrayAdapter adapter = new ArrayAdapter<User>(this, R.layout.custom_user_list_item, R.id.lblFirstName, app.users){

            @Override
            public View getView(int position, View convertView, ViewGroup parent){
                View listItemView = super.getView(position, convertView, parent);
                User currentUser = app.users.get(position);
                listItemView.setTag(currentUser);

                TextView lbl = (TextView)listItemView.findViewById(R.id.lblFirstName);
                CheckBox chk = (CheckBox)listItemView.findViewById(R.id.chkActive);

                lbl.setText(currentUser.getFirstName());
                chk.setChecked(currentUser.isActive());
                chk.setText("Active");
                chk.setTag(currentUser);

                chk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        User selectedUser = (User)view.getTag();
                        CheckBox c = (CheckBox)view;
                        selectedUser.setActive(c.isChecked());
                    }
                });

                listItemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        User selectedUser = (User)view.getTag();
                        Intent i = new Intent(UserListActivity.this, UserDetailsActivity.class);
                        i.putExtra(UserDetailsActivity.USER_ID_EXTRA, selectedUser.getId());
                        startActivity(i);
                    }
                });

                return listItemView;

            }

        };

        userListView.setAdapter(adapter);

    }
}
